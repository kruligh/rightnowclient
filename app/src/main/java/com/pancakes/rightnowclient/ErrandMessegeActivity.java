package com.pancakes.rightnowclient;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.pancakes.rightnowclient.adapter.ErrandMessegeAdapter;
import com.pancakes.rightnowclient.serverConn.RequestQueueSingleton;
import com.pancakes.rightnowclient.serverConn.ServerConfig;
import com.pancakes.rightnowclient.serverConn.dto.Messege;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ErrandMessegeActivity extends AppCompatActivity {

    private RecyclerView errandMessegeView;
    private EditText tfMessege;
    private Button btSendMessege;
    private Long errandId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_errand_message);

        errandMessegeView = (RecyclerView) findViewById(R.id.errand_messege_view);
        tfMessege = (EditText) findViewById(R.id.tfMessege) ;
      //  newMessageEt = (EditText) findViewById(R.id.new_message_et) ;
        errandMessegeView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL,false);
        errandMessegeView.setLayoutManager(mLayoutManager);

        btSendMessege = (Button) findViewById(R.id.btSendMessege);
        btSendMessege.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String requestUrl = ServerConfig.SERVER_URL + "/errand/"+ errandId + "/messages";
                Map<String, String> params = new HashMap<String, String>();

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ServerConfig.ADD_ERRAND_URL, new JSONObject(params),
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Toast.makeText(ErrandMessegeActivity.this,"Dodano zlecenie, poczekaj aż ktoś się go podejmie ;)", Toast.LENGTH_SHORT).show();
                                messegeListRequest(errandId);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if(error.networkResponse != null && error.networkResponse.statusCode == 403){
                                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getResources().getString(R.string.auth_token),MODE_PRIVATE);
                                    sharedPreferences.edit().remove(getResources().getString(R.string.auth_token)).commit();
                                    Intent intent = new Intent(ErrandMessegeActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                }else{
                                    Toast.makeText(ErrandMessegeActivity.this,"Wystąpił błąd, spróbuj ponownie ", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String>  params = new HashMap<String, String>();
                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getResources().getString(R.string.auth_token),MODE_PRIVATE);
                        String token = sharedPreferences.getString(getResources().getString(R.string.auth_token), null);
                        params.put("X-Auth-Token", token);
                        return params;
                    }
                };

                RequestQueueSingleton.getInstance(ErrandMessegeActivity.this.getApplicationContext()).addToRequestQueue(jsonObjectRequest);


            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        errandId = this.getIntent().getLongExtra("ERRAND_ID",0);
        messegeListRequest(errandId);
    }

    private void messegeListRequest(Long errandid) {
        String requestUrl = ServerConfig.SERVER_URL + "/errand/"+ errandid + "/messages";
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                requestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            ObjectMapper om = new ObjectMapper();
                            om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                            List<Messege> resultList = om.readValue(response, TypeFactory.defaultInstance().constructCollectionType(List.class, Messege.class));

                            RecyclerView.Adapter adapter = new ErrandMessegeAdapter(ErrandMessegeActivity.this, resultList);
                            errandMessegeView.swapAdapter(adapter,false);
                            errandMessegeView.setNestedScrollingEnabled(false);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Log.d("XD", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("XD", error.getMessage());
                        if(error.networkResponse != null && error.networkResponse.statusCode == 403){

                        }else{

                        }
                    }
                });
        RequestQueueSingleton.getInstance(this.getApplicationContext()).addToRequestQueue(stringRequest);
    }
}
