package com.pancakes.rightnowclient;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity {

    Button btPrincipal;
    Button btMandatory;
    Button btControlPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btPrincipal = (Button) findViewById(R.id.btPrincipalActivity);
        btMandatory = (Button) findViewById(R.id.btMandatoryActivity);
        btControlPanel = (Button) findViewById(R.id.btControlPanelActivity);
    }

    //Principal - zleceniodawca
    public void onClickPrincipal(View view) {
        //TODO stworz principal activity
        Intent intent = new Intent(this,AddErrandActivity.class);
        startActivityForResult(intent,0);
    }

    //Mandatory - zleceniobiorca
    public void onClickMandatory(View view) {
        Intent intent = new Intent(this,ErrandListActivity.class);
        startActivityForResult(intent, 0);
    }

    //Panel kontrolny
    public void onClickControlPanel(View view) {
        //TODO stworz controlPanel activity
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getResources().getString(R.string.auth_token),MODE_PRIVATE);
        sharedPreferences.edit().remove(getResources().getString(R.string.auth_token)).commit();

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
