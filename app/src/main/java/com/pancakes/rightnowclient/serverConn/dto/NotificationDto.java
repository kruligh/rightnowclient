package com.pancakes.rightnowclient.serverConn.dto;


public class NotificationDto {
    private Long offer_id;
    private Float price;

    private String state;

    private Errand errand;

    private UserInfo employee;

    public Long getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(Long offer_id) {
        this.offer_id = offer_id;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Errand getErrand() {
        return errand;
    }

    public void setErrand(Errand errand) {
        this.errand = errand;
    }

    public UserInfo getEmployee() {
        return employee;
    }

    public void setEmployee(UserInfo employee) {
        this.employee = employee;
    }
}
