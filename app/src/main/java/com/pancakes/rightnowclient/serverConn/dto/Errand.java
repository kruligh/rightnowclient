package com.pancakes.rightnowclient.serverConn.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

public class Errand {

    private Long errandId;
    private ErrandCategory category;
    private String state;
    private String title;
    private String desc;
    private Long latitude;
    private Long longitude;
    private Double price;
    private Long est_time;
    private Date date_added;
    private Date date_accepted;
    private Double score;
    private UserInfo employer;
    private UserInfo employee;
    @JsonIgnore
    private int distance;
    private boolean employer_finished;
    private boolean employee_finished;

    public Errand() {
        this.distance = 3000;
    }

    public Long getErrandId() {
        return errandId;
    }

    public void setErrandId(Long errandId) {
        this.errandId = errandId;
    }

    public ErrandCategory getCategory() {
        return category;
    }

    public void setCategory(ErrandCategory category) {
        this.category = category;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Long getLatitude() {
        return latitude;
    }

    public void setLatitude(Long latitude) {
        this.latitude = latitude;
    }

    public Long getLongitude() {
        return longitude;
    }

    public void setLongitude(Long longitude) {
        this.longitude = longitude;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getEst_time() {
        return est_time;
    }

    public void setEst_time(Long est_time) {
        this.est_time = est_time;
    }

    public Date getDate_added() {
        return date_added;
    }

    public void setDate_added(Date date_added) {
        this.date_added = date_added;
    }

    public Date getDate_accepted() {
        return date_accepted;
    }

    public void setDate_accepted(Date date_accepted) {
        this.date_accepted = date_accepted;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public UserInfo getEmployer() {
        return employer;
    }

    public void setEmployer(UserInfo employer) {
        this.employer = employer;
    }

    public UserInfo getEmployee() {
        return employee;
    }

    public void setEmployee(UserInfo employee) {
        this.employee = employee;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public boolean isEmployer_finished() {
        return employer_finished;
    }

    public void setEmployer_finished(boolean employer_finished) {
        this.employer_finished = employer_finished;
    }

    public boolean isEmployee_finished() {
        return employee_finished;
    }

    public void setEmployee_finished(boolean employee_finished) {
        this.employee_finished = employee_finished;
    }
}
