package com.pancakes.rightnowclient.serverConn.dto;


import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrandCategory {

    @JsonProperty("cat_id")
    private long id;

    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
