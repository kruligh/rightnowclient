package com.pancakes.rightnowclient.serverConn.dto;

import java.util.Date;

/**
 * Created by Lenovo on 23.04.2017.
 */

public class Messege {
    private Long message_id;
    private Errand errand;
    private UserInfo user;
    private Date date_added;
    private String text;

    public Long getMessage_id() {
        return message_id;
    }

    public void setMessage_id(Long message_id) {
        this.message_id = message_id;
    }

    public Errand getErrand() {
        return errand;
    }

    public void setErrand(Errand errand) {
        this.errand = errand;
    }

    public UserInfo getUser() {
        return user;
    }

    public void setUser(UserInfo user) {
        this.user = user;
    }

    public Date getDate_added() {
        return date_added;
    }

    public void setDate_added(Date date_added) {
        this.date_added = date_added;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
