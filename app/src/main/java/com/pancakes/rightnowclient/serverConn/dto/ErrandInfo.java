package com.pancakes.rightnowclient.serverConn.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrandInfo {

    @JsonProperty(value = "errandId")
    private long id;
    private String title;
    private ErrandCategory category;

    @JsonIgnore
    private int distance;

    public ErrandInfo() {
        this.distance = 3000;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public ErrandCategory getCategory() {
        return category;
    }

    public void setCategory(ErrandCategory category) {
        this.category = category;
    }
}
