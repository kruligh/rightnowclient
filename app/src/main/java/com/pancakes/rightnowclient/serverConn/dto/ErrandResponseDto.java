package com.pancakes.rightnowclient.serverConn.dto;

/**
 * Created by kroli on 23.04.2017.
 */

public class ErrandResponseDto {
    private Errand errand;
    private boolean isOwner;

    public Errand getErrand() {
        return errand;
    }

    public void setErrand(Errand errand) {
        this.errand = errand;
    }

    public boolean isOwner() {
        return isOwner;
    }

    public void setOwner(boolean owner) {
        isOwner = owner;
    }
}
