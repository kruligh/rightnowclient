package com.pancakes.rightnowclient.serverConn.dto;


public class UserInfo {

    private String login;
    private String display_name;
    private String email;
    private Float score;
    private Long no_scores;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }

    public Long getNo_scores() {
        return no_scores;
    }

    public void setNo_scores(Long no_scores) {
        this.no_scores = no_scores;
    }
}
