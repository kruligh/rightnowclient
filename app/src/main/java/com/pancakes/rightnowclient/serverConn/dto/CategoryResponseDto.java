package com.pancakes.rightnowclient.serverConn.dto;

/**
 * Created by kroli on 23.04.2017.
 */

public class CategoryResponseDto {

    private long cat_id;
    private String name;

    public long getCat_id() {
        return cat_id;
    }

    public void setCat_id(long cat_id) {
        this.cat_id = cat_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
