package com.pancakes.rightnowclient.serverConn;

public final class ServerConfig {
    public static final String SERVER_URL = "http://137.74.164.100:8080";
//http://192.168.173.2:8080 137.74.164.100:8080
    public static final String GET_ERRAND_LIST_URL = SERVER_URL + "/errand/list";
    public static final String GET_ERRAND_URL = SERVER_URL +"/errand";
    public static final String LOGIN_URL = SERVER_URL + "/user/session/login";
    public static final String ADD_ERRAND_URL = SERVER_URL + "/errand/add";
}
