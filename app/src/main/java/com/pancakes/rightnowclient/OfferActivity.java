package com.pancakes.rightnowclient;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class OfferActivity extends AppCompatActivity {

    TextView tvUsername;
    TextView tvNickname;
    TextView tvRank;
    TextView tvState;
    TextView tvCategory;
    TextView tvDate;
    TextView tvPrice;
    TextView tvDescription;
    Button btLocation;
    Button btState;
    Button btUserAvatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer);

        tvUsername = (TextView) findViewById(R.id.tvUsername);
        tvNickname = (TextView) findViewById(R.id.tvNickName);
        tvRank = (TextView) findViewById(R.id.tvRank);
        tvState = (TextView) findViewById(R.id.tvState);
        tvCategory = (TextView) findViewById(R.id.tvCategory);
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvPrice = (TextView) findViewById(R.id.tvPrice);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        btLocation = (Button) findViewById(R.id.btLocation);
        btState = (Button) findViewById(R.id.btState);
        btUserAvatar = (Button) findViewById(R.id.btUserAvatar);

    }
}
