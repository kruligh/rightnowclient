package com.pancakes.rightnowclient;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pancakes.rightnowclient.serverConn.RequestQueueSingleton;
import com.pancakes.rightnowclient.serverConn.ServerConfig;
import com.pancakes.rightnowclient.serverConn.dto.Errand;
import com.pancakes.rightnowclient.serverConn.dto.ErrandResponseDto;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ErrandActivity extends AppCompatActivity {

    TextView tvUsername;
    TextView tvNickname;
    TextView tvRank;
    TextView tvState;
    TextView tvCategory;
    TextView tvDate;
    TextView tvPrice;
    TextView tvDescription;
    Button btLocation;
    Button btState;
    Button btUserAvatar;
    RelativeLayout layoutContact;
    Button btMessage;

    private ErrandResponseDto errandResponseDto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_errand);
        
        tvUsername = (TextView) findViewById(R.id.tvUsername);
        tvNickname = (TextView) findViewById(R.id.tvNickName);
        tvRank = (TextView) findViewById(R.id.tvRank);
        tvState = (TextView) findViewById(R.id.tvState);
        tvCategory = (TextView) findViewById(R.id.tvCategory);
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvPrice = (TextView) findViewById(R.id.tvPrice);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        btLocation = (Button) findViewById(R.id.btLocation);
        btState = (Button) findViewById(R.id.btState);
        btUserAvatar = (Button) findViewById(R.id.btUserAvatar);
        layoutContact = (RelativeLayout) findViewById((R.id.layoutContact));
        btMessage = (Button) findViewById(R.id.btMessage);

        btMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ErrandActivity.this, ErrandMessegeActivity.class);
                intent.putExtra("ERRAND_ID",errandResponseDto.getErrand().getErrandId());
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        Long errandId = this.getIntent().getLongExtra("ERRAND_ID",0);

        if (errandId == 0){
            //// TODO: 22.04.2017
        }
        errandRequest(errandId);
    }

    private void setErrandData(Errand errand) {
        layoutContact.setVisibility(View.VISIBLE);
        if (errandResponseDto.isOwner()) {
            if (errand.getState().equals("ACTIVE")) {
                //Jestem wlascicielem i nikt nie przyjal zlecenia
                layoutContact.setEnabled(false);
                layoutContact.setVisibility(View.GONE);

            } else {
                //Jestem wlascicielem i ktos juz przyjal zlecenie (employee)
                tvUsername.setText(errand.getEmployee().getDisplay_name());
                btUserAvatar.setText(errand.getEmployee().getDisplay_name().substring(0, 1).toUpperCase());
                tvNickname.setText(errand.getEmployee().getLogin());
                tvRank.setText(errand.getEmployee().getScore().toString());
            }
        } else {
            //Jestem polakiem i pracuje dla Janusza Tracza
            tvUsername.setText(errand.getEmployer().getDisplay_name());
            btUserAvatar.setText(errand.getEmployer().getDisplay_name().substring(0, 1).toUpperCase());
            tvNickname.setText(errand.getEmployer().getLogin());
            tvRank.setText(errand.getEmployer().getScore().toString());
        }

        tvState.setText("STATUS: " + errand.getState().toUpperCase());
        tvCategory.setText("KATEGORIA: " + errand.getCategory().getName());
        tvDate.setText("DATA: " + new SimpleDateFormat("HH EEEE, dd-MMM-yyyy", new Locale("pl","PL")).format(new Date(errand.getDate_added().getTime())));
        tvPrice.setText("WYNAGRODZENIE: " + errand.getPrice().toString() + "zł");
        tvDescription.setText(errand.getDesc());

        double latitude = ((double) errand.getLatitude())/10000;
        double longitude = ((double) errand.getLongitude())/10000;
        String location;
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            location = addresses.get(0).getLocality() + " " + addresses.get(0).getAddressLine(0);

            if (location.length() > 64) {
                location = location.substring(0, 64);
            }
            btLocation.setText(location);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void errandRequest(final Long errandId) {
        String requestUrl = ServerConfig.GET_ERRAND_URL + "/"+ errandId;

        Log.d("errdandListRequest", requestUrl);

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                requestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            ObjectMapper om = new ObjectMapper();
                            om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                            errandResponseDto = om.readValue(response, ErrandResponseDto.class);

                            Errand errand = errandResponseDto.getErrand();
                            setErrandData(errand);

                            //State button
                            btState.setVisibility(View.VISIBLE);
                            if (errand.getState().equals("ACTIVE")) {
                                btState.setText(getString(R.string.errand_offer_to_errand));
                                btState.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        //todo zrobić popupa w ktorym mozna wpisać cene za ktora mozna sie podjac tego zadania i potem ta cene umieścić do metody poniżej i ją wysłać  (wysłać moge ja)
                                        offerToErrandRequest();
                                    }
                                });
                            }else if(errand.getState().equals("INPROGRESS")){

                                if((errandResponseDto.isOwner() && errand.isEmployer_finished()) || (!errandResponseDto.isOwner() && errand.isEmployee_finished())){
                                    btState.setText(getString(R.string.errand_waintin_for_finish));
                                    btState.setClickable(false);
                                }else{
                                    btState.setText(getString(R.string.errand_errand_finish));
                                    btState.setClickable(true);
                                    btState.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            //todo zrobic popupa w ktorym podaje się ocene drugiej osoby np od 1-5 i tak samo do metody a ja potem to wysle
                                            finishErrandRequest();
                                        }
                                    });
                                }
                            }else if(errand.getState().equals("DONE") || errand.getState().equals("CANCELLED")){
                                btState.setClickable(false);
                                btState.setText(getString(R.string.errand_errand_finished));
                            }


                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error.networkResponse != null && error.networkResponse.statusCode == 403){
                            on403();
                        }
                    }
                }){   @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String, String>  params = new HashMap<String, String>();
            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getResources().getString(R.string.auth_token),MODE_PRIVATE);
            String token = sharedPreferences.getString(getResources().getString(R.string.auth_token), null);
            params.put("X-Auth-Token", token);
            return params;
        }
    };
        RequestQueueSingleton.getInstance(this.getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void finishErrandRequest() {
        String requestUrl = ServerConfig.SERVER_URL + "/errand/"+ errandResponseDto.getErrand().getErrandId()+"/offer";

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                requestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Toast.makeText(ErrandActivity.this,getString(R.string.errand_offer_to_errand_sent),Toast.LENGTH_SHORT).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if(error.networkResponse != null && error.networkResponse.statusCode == 403){
on403();
                        }else{
                            Toast.makeText(ErrandActivity.this, R.string.errand_offer_to_errand_failed,Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getResources().getString(R.string.auth_token),MODE_PRIVATE);
                String token = sharedPreferences.getString(getResources().getString(R.string.auth_token), null);
                params.put("X-Auth-Token", token);
                return params;
            }
        };

        RequestQueueSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void offerToErrandRequest() {
        String requestUrl = ServerConfig.SERVER_URL + "/errand/"+ errandResponseDto.getErrand().getErrandId()+"/offer";

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                requestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Toast.makeText(ErrandActivity.this,getString(R.string.errand_offer_to_errand_sent),Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if(error.networkResponse != null && error.networkResponse.statusCode == 403){

                        }else{
                            Toast.makeText(ErrandActivity.this, R.string.errand_offer_to_errand_failed,Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getResources().getString(R.string.auth_token),MODE_PRIVATE);
                String token = sharedPreferences.getString(getResources().getString(R.string.auth_token), null);
                params.put("X-Auth-Token", token);
                params.put("Content-Type", "application/json" );
                return params;
            }
        };

        RequestQueueSingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    private void on403(){
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getResources().getString(R.string.auth_token),MODE_PRIVATE);
        sharedPreferences.edit().remove(getResources().getString(R.string.auth_token)).commit();
        Intent intent = new Intent(ErrandActivity.this, LoginActivity.class);
        startActivity(intent);
    }
}
