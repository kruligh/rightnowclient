package com.pancakes.rightnowclient;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.pancakes.rightnowclient.adapter.ErrandListAdapter;
import com.pancakes.rightnowclient.adapter.NotificationListAdapter;
import com.pancakes.rightnowclient.serverConn.RequestQueueSingleton;
import com.pancakes.rightnowclient.serverConn.ServerConfig;
import com.pancakes.rightnowclient.serverConn.dto.NotificationDto;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotificationActivity extends AppCompatActivity {
    private RecyclerView errandListRecyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
    }

    @Override
    protected void onResume() {
        super.onResume();
        errandListRecyclerView = (RecyclerView) findViewById(R.id.notification_list_recycler_view);
        errandListRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL,false);
        errandListRecyclerView.setLayoutManager(mLayoutManager);

        notificationsRequest();
    }

    private void notificationsRequest() {
        final String requestUrl = ServerConfig.SERVER_URL + "/user/offers/list";

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                requestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            ObjectMapper om = new ObjectMapper();
                            om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                            List<NotificationDto> resultList = om.readValue(response, TypeFactory.defaultInstance().constructCollectionType(List.class, NotificationDto.class));

                            RecyclerView.Adapter adapter = new NotificationListAdapter(NotificationActivity.this, resultList,errandListRecyclerView);
                            errandListRecyclerView.swapAdapter(adapter,false);
                            errandListRecyclerView.setNestedScrollingEnabled(false);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Log.d("XD", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("XD", error.getMessage());
                        if(error.networkResponse != null && error.networkResponse.statusCode == 403){

                        }else{

                        }
                    }
                })  {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getResources().getString(R.string.auth_token),MODE_PRIVATE);
                String token = sharedPreferences.getString(getResources().getString(R.string.auth_token), null);
                params.put("X-Auth-Token", token);
                return params;
            }
        };

        RequestQueueSingleton.getInstance(this.getApplicationContext()).addToRequestQueue(stringRequest);
    }
}
