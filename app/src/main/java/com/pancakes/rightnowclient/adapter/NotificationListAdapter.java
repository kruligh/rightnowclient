package com.pancakes.rightnowclient.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pancakes.rightnowclient.AddErrandActivity;
import com.pancakes.rightnowclient.ErrandActivity;
import com.pancakes.rightnowclient.LoginActivity;
import com.pancakes.rightnowclient.NotificationActivity;
import com.pancakes.rightnowclient.R;
import com.pancakes.rightnowclient.serverConn.RequestQueueSingleton;
import com.pancakes.rightnowclient.serverConn.ServerConfig;
import com.pancakes.rightnowclient.serverConn.dto.ErrandInfo;
import com.pancakes.rightnowclient.serverConn.dto.NotificationDto;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by kroli on 23.04.2017.
 */

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.ViewHolder> {

private Context mContext;
private List<NotificationDto> list;
    RecyclerView recyclerView;

public NotificationListAdapter(Context mContext, List<NotificationDto> list , RecyclerView recyclerView) {
        this.mContext = mContext;
        this.list = list;
    this.recyclerView = recyclerView;
        }

public static class ViewHolder extends RecyclerView.ViewHolder {
    public TextView titleTextView;
    public TextView priceTextView;
    public TextView employeeNameText;
    public Button acceptButton;
    public Button delinceButton;
    public View divider;
    public long offerId;
    public String employeeLogin;

    public ViewHolder(LinearLayout l, final Context mContext) {
        super(l);

        titleTextView = (TextView) l.findViewById(R.id.notification_list_title_tv);
        priceTextView = (TextView) l.findViewById(R.id.notification_list_price_tv);
        employeeNameText = (TextView)l.findViewById(R.id.notification_list_employee_tv);
        acceptButton = (Button) l.findViewById(R.id.notification_list_accept_button);
        delinceButton = (Button) l.findViewById(R.id.notification_list_delince_button);
        divider = (View) l.findViewById(R.id.notification_list_divider_view);
    }
}

    @Override
    public NotificationListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LinearLayout l = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_norification_list, parent, false);

        NotificationListAdapter.ViewHolder vh = new NotificationListAdapter.ViewHolder(l,mContext);
        return vh;
    }

    @Override
    public void onBindViewHolder(NotificationListAdapter.ViewHolder holder, int position) {
        NotificationDto notificationDto = list.get(position);

        holder.employeeLogin = notificationDto.getEmployee().getLogin();
        holder.titleTextView.setText(notificationDto.getErrand().getTitle());
        holder.priceTextView.setText(Float.toString(notificationDto.getPrice()));
        holder.employeeNameText.setText(notificationDto.getEmployee().getDisplay_name());

        holder.acceptButton.setTag(notificationDto);
        holder.acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptOfferRequest(((NotificationDto)v.getTag()));
            }
        });

        holder.delinceButton.setTag(notificationDto);
        holder.delinceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delinceOfferRequest(((NotificationDto)v.getTag()));
            }
        });

        int visible = (position == getItemCount()-1) ? View.GONE : View.VISIBLE;
        holder.divider.setVisibility(visible);
    }

    private void delinceOfferRequest(NotificationDto notificationDto) {
        String stringUrl = ServerConfig.SERVER_URL + "/errand/" + notificationDto.getErrand().getErrandId() + "/offer/" + notificationDto.getOffer_id() + "/decline";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, stringUrl, new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(mContext, "Odrzucono.",Toast.LENGTH_LONG).show();
                        RecyclerView.Adapter adapter = new NotificationListAdapter(mContext, Collections.EMPTY_LIST, recyclerView);
                        recyclerView.swapAdapter(adapter,false);
                        recyclerView.setNestedScrollingEnabled(false);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(mContext, "Odrzucono",Toast.LENGTH_LONG).show();
                        RecyclerView.Adapter adapter = new NotificationListAdapter(mContext, Collections.EMPTY_LIST, recyclerView);
                        recyclerView.swapAdapter(adapter,false);
                        recyclerView.setNestedScrollingEnabled(false);

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                SharedPreferences sharedPreferences = mContext.getSharedPreferences(mContext.getResources().getString(R.string.auth_token),MODE_PRIVATE);
                String token = sharedPreferences.getString(mContext.getResources().getString(R.string.auth_token), null);
                params.put("X-Auth-Token", token);
                return params;
            }
        };

        RequestQueueSingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }

    private void acceptOfferRequest(NotificationDto notificationDto) {

        final long temp= notificationDto.getErrand().getErrandId();
        String stringUrl = ServerConfig.SERVER_URL + "/errand/" + notificationDto.getErrand().getErrandId() + "/offer/" + notificationDto.getOffer_id() + "/accept";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, stringUrl, new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(mContext, "Oferta została przekazana do realizacji.",Toast.LENGTH_LONG);
                        RecyclerView.Adapter adapter = new NotificationListAdapter(mContext, Collections.EMPTY_LIST, recyclerView);
                        recyclerView.swapAdapter(adapter,false);
                        recyclerView.setNestedScrollingEnabled(false);

                        Intent intent = new Intent(mContext,ErrandActivity.class);
                        intent.putExtra("ERRAND_ID", temp);
                        mContext.startActivity(intent);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(mContext, "Oferta została przekazana do realizacji",Toast.LENGTH_LONG);
                        RecyclerView.Adapter adapter = new NotificationListAdapter(mContext, Collections.EMPTY_LIST, recyclerView);
                        recyclerView.swapAdapter(adapter,false);
                        recyclerView.setNestedScrollingEnabled(false);

                        Intent intent = new Intent(mContext,ErrandActivity.class);
                        intent.putExtra("ERRAND_ID", temp);
                        mContext.startActivity(intent);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                SharedPreferences sharedPreferences = mContext.getSharedPreferences(mContext.getResources().getString(R.string.auth_token),MODE_PRIVATE);
                String token = sharedPreferences.getString(mContext.getResources().getString(R.string.auth_token), null);
                params.put("X-Auth-Token", token);
                return params;
            }
        };

        RequestQueueSingleton.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}