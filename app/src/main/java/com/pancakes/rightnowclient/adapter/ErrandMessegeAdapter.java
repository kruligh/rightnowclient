package com.pancakes.rightnowclient.adapter;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.pancakes.rightnowclient.R;
import com.pancakes.rightnowclient.serverConn.dto.Messege;
import java.util.List;


public class ErrandMessegeAdapter extends RecyclerView.Adapter<ErrandMessegeAdapter.ViewHolder> {

    private Context mContext;
    private List<Messege> messegeList;


    public ErrandMessegeAdapter(Context mContext, List<Messege> messegeList) {
        this.mContext = mContext;
        this.messegeList = messegeList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView userTextView;
        public TextView dateTextView;
        public TextView messegeTextView;
        public long errandId;


        public ViewHolder(LinearLayout l, final Context mContext) {
            super(l);
            userTextView = (TextView) l.findViewById(R.id.userTv);
            dateTextView = (TextView) l.findViewById(R.id.dateTv);
            messegeTextView = (TextView) l.findViewById(R.id.messageTv);
        }
    }

    @Override
    public ErrandMessegeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LinearLayout l = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_errand_list, parent, false);

        ErrandMessegeAdapter.ViewHolder vh = new ErrandMessegeAdapter.ViewHolder(l,mContext);
        return vh;
    }

    @Override
    public void onBindViewHolder(ErrandMessegeAdapter.ViewHolder holder, int position) {
        Messege messege = messegeList.get(position);
        holder.userTextView.setText(messege.getUser().getDisplay_name());
        holder.dateTextView.setText(messege.getDate_added().toString());
        holder.messegeTextView.setText(messege.getText());
        holder.errandId = messege.getErrand().getErrandId();
    }

    @Override
    public int getItemCount() {
        return messegeList.size();
    }
}
