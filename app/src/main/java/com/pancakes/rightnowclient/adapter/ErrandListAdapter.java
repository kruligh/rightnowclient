package com.pancakes.rightnowclient.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pancakes.rightnowclient.ErrandActivity;
import com.pancakes.rightnowclient.R;
import com.pancakes.rightnowclient.serverConn.dto.ErrandInfo;

import java.util.List;



public class ErrandListAdapter extends RecyclerView.Adapter<ErrandListAdapter.ViewHolder> {

    private Context mContext;
    private List<ErrandInfo> errandList;

    public ErrandListAdapter(Context mContext, List<ErrandInfo> errandList) {
        this.mContext = mContext;
        this.errandList = errandList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView titleTextView;
        public TextView distanceTextView;
        public TextView categoryTextView;
        public View divider;
        public long errandId;

        public ViewHolder(LinearLayout l, final Context mContext) {
            super(l);
            l.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ErrandActivity.class);
                    intent.putExtra("ERRAND_ID", errandId);
                    mContext.startActivity(intent);
                }
            });
            titleTextView = (TextView) l.findViewById(R.id.errand_list_title_tv);
            distanceTextView = (TextView) l.findViewById(R.id.errand_list_distance_tv);
            categoryTextView = (TextView) l.findViewById(R.id.errand_list_category_tv);
            divider = (View) l.findViewById(R.id.errand_list_divider_view);
        }
    }

    @Override
    public ErrandListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LinearLayout l = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_errand_list, parent, false);

        ViewHolder vh = new ViewHolder(l,mContext);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ErrandInfo errand = errandList.get(position);
        holder.titleTextView.setText(errand.getTitle());
        holder.categoryTextView.setText(errand.getCategory().getName());
        holder.distanceTextView.setText(Integer.toString(errand.getDistance()));
        holder.errandId = errand.getId();
        int visible = (position == getItemCount()-1) ? View.GONE : View.VISIBLE;
        holder.divider.setVisibility(visible);

    }

    @Override
    public int getItemCount() {
        return errandList.size();
    }
}