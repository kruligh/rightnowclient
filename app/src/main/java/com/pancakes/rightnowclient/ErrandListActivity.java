package com.pancakes.rightnowclient;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.pancakes.rightnowclient.adapter.ErrandListAdapter;
import com.pancakes.rightnowclient.serverConn.RequestQueueSingleton;
import com.pancakes.rightnowclient.serverConn.ServerConfig;
import com.pancakes.rightnowclient.serverConn.dto.ErrandInfo;

import java.io.IOException;
import java.util.List;

public class ErrandListActivity extends AppCompatActivity {
    private RecyclerView errandListRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_errand_list);

        errandListRecyclerView = (RecyclerView) findViewById(R.id.errand_list_recycler_view);
        errandListRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL,false);
        errandListRecyclerView.setLayoutManager(mLayoutManager);

    }

    @Override
    protected void onResume() {
        super.onResume();
        String[] tmep = {"NAPRAWA","ZAKUPY"};
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getResources().getString(R.string.settings_shared_pref_name),MODE_PRIVATE);
        int distance = sharedPreferences.getInt(getResources().getString(R.string.settings_distance),10000);

        errandListRequest(50.048336, 19.961495, distance, tmep );
    }

    private void errandListRequest(double lat, double lon, int distance, String[] categories) {

        long latilong = (long)lat*10000;
        long lonlong = (long) lon*10000;
        String requestUrl = ServerConfig.GET_ERRAND_LIST_URL + "?lat=" +  latilong + "&lon="+lonlong +"&dist=" + distance;

        if(categories.length>0){
            requestUrl = requestUrl +  "&cat=" + categories[0];

            for(int i=1; i<categories.length; i++){
                requestUrl = requestUrl +  ","+categories[i];
            }

        }

        Log.d("XD",requestUrl);

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                requestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            ObjectMapper om = new ObjectMapper();
                            om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                            List<ErrandInfo> resultList = om.readValue(response, TypeFactory.defaultInstance().constructCollectionType(List.class, ErrandInfo.class));

                            RecyclerView.Adapter adapter = new ErrandListAdapter(ErrandListActivity.this, resultList);
                            errandListRecyclerView.swapAdapter(adapter,false);
                            errandListRecyclerView.setNestedScrollingEnabled(false);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Log.d("XD", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("XD", error.getMessage());
                        if(error.networkResponse != null && error.networkResponse.statusCode == 403){

                        }else{

                        }
                    }
        });

        RequestQueueSingleton.getInstance(this.getApplicationContext()).addToRequestQueue(stringRequest);
    }
}
