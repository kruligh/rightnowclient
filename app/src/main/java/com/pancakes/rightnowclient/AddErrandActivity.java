package com.pancakes.rightnowclient;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.pancakes.rightnowclient.adapter.ErrandListAdapter;
import com.pancakes.rightnowclient.serverConn.RequestQueueSingleton;
import com.pancakes.rightnowclient.serverConn.ServerConfig;
import com.pancakes.rightnowclient.serverConn.dto.CategoryResponseDto;
import com.pancakes.rightnowclient.serverConn.dto.ErrandCategory;
import com.pancakes.rightnowclient.serverConn.dto.ErrandInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddErrandActivity extends AppCompatActivity {

    EditText tfTitle;
    EditText tfDescription;
    EditText tfPrice;
    EditText tfTime;
    Spinner spCategory;
    Button btPlacePicker;
    List<CategoryResponseDto> categories;
    long latitude,longitude;

    int PLACE_PICKER_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_errand);

        tfTitle = (EditText) findViewById(R.id.tfTitle);
        tfDescription = (EditText) findViewById(R.id.tfDescription);
        tfPrice = (EditText) findViewById(R.id.tfPrice);
        tfTime = (EditText) findViewById(R.id.tfTime);
        spCategory = (Spinner) findViewById(R.id.spCategory);
        btPlacePicker = (Button) findViewById(R.id.btPlacePicker);

        categoryRequest();
    }

    private void categoryRequest() {

        final String requestUrl = ServerConfig.SERVER_URL+"/category/list";

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                requestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            ObjectMapper om = new ObjectMapper();
                            om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                            categories = om.readValue(response, TypeFactory.defaultInstance().constructCollectionType(List.class, CategoryResponseDto.class));
                            List<String> list = new ArrayList<>();

                            for(CategoryResponseDto item: categories){
                                list.add(item.getName());
                            }


                            ArrayAdapter<String> spCatAdp = new ArrayAdapter<String>(AddErrandActivity.this, android.R.layout.simple_list_item_1,list);
                            spCatAdp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spCategory.setAdapter(spCatAdp);


                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Log.d("XD", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getResources().getString(R.string.auth_token),MODE_PRIVATE);
                        sharedPreferences.edit().remove(getResources().getString(R.string.auth_token)).commit();
                        Intent intent = new Intent(AddErrandActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                });

        RequestQueueSingleton.getInstance(this.getApplicationContext()).addToRequestQueue(stringRequest);
    }

    //PlacePicker
    public void onClickPlacePicker(View view) {
        selectLocation();
    }

    private void selectLocation() {

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this); //TODO popraw jak umiesz
                String toastMsg = String.format("%s", place.getAddress());
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();

                latitude = (long) (place.getLatLng().latitude * 10000);
                longitude =(long) (place.getLatLng().longitude * 10000);

                //Ustaw ładnie button
                updatePlaceLabel(toastMsg);
            }
        }
    }

    private void updatePlaceLabel(String name) {
        if (name.length() > 64) {
            name = name.substring(0, 64); //a nie 63? no nie wiem skad mam wiedziec
        }
        btPlacePicker.setText(name);
    }

    public void onClickAddOffer(View view) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("cat_id", Long.toString(categories.get(spCategory.getSelectedItemPosition()).getCat_id()));
        params.put("title", tfTitle.getText().toString());
        params.put("description", tfDescription.getText().toString());
        params.put("price", tfPrice.getText().toString());
        params.put("lat", Long.toString(latitude));
        params.put("lon", Long.toString(longitude));
        params.put("est_time", tfTime.getText().toString());


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ServerConfig.ADD_ERRAND_URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                    Toast.makeText(AddErrandActivity.this,"Dodano zlecenie, poczekaj aż ktoś się go podejmie ;)", Toast.LENGTH_SHORT).show();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error.networkResponse != null && error.networkResponse.statusCode == 403){
                            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getResources().getString(R.string.auth_token),MODE_PRIVATE);
                            sharedPreferences.edit().remove(getResources().getString(R.string.auth_token)).commit();

                            Intent intent = new Intent(AddErrandActivity.this, LoginActivity.class);
                                startActivity(intent);


                        }else{
                            Toast.makeText(AddErrandActivity.this,"Wystąpił błąd, spróbuj ponownie ", Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String>  params = new HashMap<String, String>();
                            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getResources().getString(R.string.auth_token),MODE_PRIVATE);
                            String token = sharedPreferences.getString(getResources().getString(R.string.auth_token), null);
                            params.put("X-Auth-Token", token);
                            return params;
                        }
                    };

        RequestQueueSingleton.getInstance(this.getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

}
