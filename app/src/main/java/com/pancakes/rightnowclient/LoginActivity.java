package com.pancakes.rightnowclient;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.pancakes.rightnowclient.serverConn.RequestQueueSingleton;
import com.pancakes.rightnowclient.serverConn.ServerConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class LoginActivity extends AppCompatActivity {

    private EditText loginET;
    private EditText passET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.loginET = (EditText) findViewById(R.id.login_login_edit_text);
        this.passET = (EditText) findViewById(R.id.login_password_edit_text);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getResources().getString(R.string.auth_token),MODE_PRIVATE);
        if(sharedPreferences.contains(getString(R.string.auth_token))){
            goToHome();
        }
    }

    public void onClickSignIn(View view) {
        Toast.makeText(this,"Not implemented", Toast.LENGTH_SHORT).show();
    }

    public void onClickLogin(View view){

        String loginString = loginET.getText().toString();
        String passwordString = passET.getText().toString();

        //todo  walidacja bedzie tutaj potrzebna - np czy wpisane dane

        loginRequest(loginString,passwordString);

    }

    private void loginRequest(String loginString, String passwordString) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("login",loginString);
        params.put("password",passwordString);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ServerConfig.LOGIN_URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getResources().getString(R.string.auth_token),MODE_PRIVATE);
                        try {
                            sharedPreferences.edit().putString(getResources().getString(R.string.auth_token),response.getString("token")).apply();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        NotificationEventReceiver.setupAlarm(getApplicationContext());
                        goToHome();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error.networkResponse != null && error.networkResponse.statusCode == 403){
                            passET.setError(getString(R.string.login_invalid_data));
                        }else{
                            passET.setError(getString(R.string.unknown_error));
                        }
                    }
                });

        RequestQueueSingleton.getInstance(this.getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    private void goToHome() {
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

}
